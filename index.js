const express = require('express');
const bodyParser = require('body-parser');
const oauthSignature = require('oauth-signature');
const app = express();
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`); // node index.js
});

app.use(bodyParser.urlencoded({ extended: true }));

app.post('/lti', (req, res) => {
  const consumerSecret = 'YOUR_SECRET_KEY'; // your consumer secret
  const method = 'POST'; // or GET. fixed. method
  const action = 'https://todo' // fixed. your url;
  const signatureReceive = req.body.oauth_signature;
  delete req.body.oauth_signature;

  // Generate the signature
  const signature = oauthSignature.generate(
    method,
    action,
    req.body,
    consumerSecret,
    null,
    { encodeSignature: false }
  );

  // Check that the signature is correct
  if (signature !== signatureReceive) {
    return res.status(401).send('Invalid LTI request: signature is invalid');
  }

  // @TODO With a valid signature, the edtech must now log the user
  // into its solution and redirect them to the platform.

  res.send('LTI request processed successfully!');
});
